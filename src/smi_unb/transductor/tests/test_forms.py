import mock

from django.test import TestCase
from django.utils import timezone
from django.utils.translation import ugettext as _

from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.forms import EnergyForm
from smi_unb.transductor.models import TransductorModel


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(None, 200)


class EnergyTransductorForm(TestCase):
    def setUp(self):
        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_valid_form(self, mock):
        data = {
            'building': self.building.id,
            'model': self.t_model.id,
            'name': 'transductor test',
            'ip_address': "111.111.111.111"
        }

        form = EnergyForm(data=data, building=self.building)

        self.assertTrue(form.is_valid())

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_invalid_form(self, mock):
        data = {
            'serie_number': '',
            'ip_address': "1",
            'model': ''
        }

        form = EnergyForm(data=data, building=self.building)

        self.assertFalse(form.is_valid())

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_invalid_form_server_connection(self, mock):
        data = {
            'serie_number': '',
            'ip_address': "1",
            'model': ''
        }

        form = EnergyForm(data=data, building=self.building)

        self.assertFalse(form.is_valid())

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_energyform_valid_calibration_date(self, mock):
        now = timezone.now()

        data = {
            'building': self.building.id,
            'model': self.t_model.id,
            'name': 'transductor test',
            'ip_address': "111.111.111.111",
            'calibration_date': now
        }

        form = EnergyForm(data=data, building=self.building)
        self.assertTrue(form.is_valid())

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_energyform_invalid_calibration_date(self, mock):
        later = timezone.now() + timezone.timedelta(days=1)

        data = {
            'building': self.building.id,
            'model': self.t_model.id,
            'name': 'transductor test',
            'ip_address': "111.111.111.111",
            'calibration_date': later
        }

        form = EnergyForm(data=data, building=self.building)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            [_('Data de calibração não pode ser depois do horário atual.')],
            form.errors['calibration_date']
        )
