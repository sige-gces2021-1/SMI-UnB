import mock
import numpy
import pandas

from django.test import TestCase
from django.utils import timezone
from django.utils.translation import ugettext as _
from matplotlib.figure import Figure

from smi_unb.buildings.models import Building
from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.transductor.models import TransductorModel, EnergyTransductor, \
                                       EnergyMeasurements
from smi_unb.report.exceptions import NoMeasurementsFoundException
from smi_unb.report.utils import GraphDateManager, GraphPlotManager, \
                                 GraphDataManager, GraphUtils


class TestReportViews(TestCase):
    def setUp(self):
        self.building = self.create_building()

        self.t_model = TransductorModel.objects.create(
            name="TR 4020",
            transport_protocol="UDP",
            serial_protocol="Modbus RTU",
            register_addresses=[[68, 0], [70, 1]],
        )

        self.transductor = EnergyTransductor.objects.create(
            building=self.building,
            model=self.t_model,
            name="transductor test",
            ip_address="111.111.111.111"
        )

    '''
    Utility Methods
    '''
    def create_energy_measurements(
        self, max_range, initial_date, **kwargs
    ):
        initial_date_copy = initial_date

        for hour in range(0, max_range):
            e_measurement = EnergyMeasurements()
            e_measurement.transductor = self.transductor

            e_measurement.voltage_a = 122.875 + hour
            e_measurement.voltage_b = 122.784 + hour
            e_measurement.voltage_c = 121.611 + hour

            e_measurement.current_a = 22.831 + hour
            e_measurement.current_b = 17.187 + hour
            e_measurement.current_c = 3.950 + hour

            e_measurement.active_power_a = 2.794 + hour
            e_measurement.active_power_b = 1.972 + hour
            e_measurement.active_power_c = 3.950 + hour

            e_measurement.reactive_power_a = -0.251 - hour
            e_measurement.reactive_power_b = -0.752 - hour
            e_measurement.reactive_power_c = -1.251 - hour

            e_measurement.apparent_power_a = 2.805 + hour
            e_measurement.apparent_power_b = 2.110 + hour
            e_measurement.apparent_power_c = 4.144 + hour

            e_measurement.collection_date = initial_date_copy

            initial_date_copy += timezone.timedelta(**kwargs)

            e_measurement.save()

    '''
    GraphDateManager Tests
    '''
    def test_get_previous_days(self):
        date_manager = GraphDateManager()

        date = timezone.now().replace(month=1, day=8)

        week_before = date - timezone.timedelta(days=7)

        self.assertEqual(
            [1, 2, 3, 4, 5, 6, 7],
            date_manager.get_previous_days(week_before)
        )

    def test_get_current_daily_dates(self):
        date_manager = GraphDateManager()

        current_date = timezone.now()

        initial_current_date = current_date.replace(
            hour=00,
            minute=00,
            second=00,
            microsecond=00
        )

        self.assertEqual(
            [initial_current_date, current_date],
            date_manager.get_initial_and_current_dates(current_date)
        )

    def test_set_past_dates(self):
        date_manager = GraphDateManager()

        date = timezone.now().replace(month=1, day=2)

        initial_date, final_date = date_manager.get_past_dates(1, date)

        time_diff = final_date - initial_date

        self.assertEqual(1, time_diff.days)
        self.assertEqual(0, time_diff.seconds)

    @mock.patch.object(
        GraphDateManager, 'get_past_dates',
        return_value='any date 2', autospec=True)
    @mock.patch.object(
        GraphDateManager, 'get_initial_and_current_dates',
        return_value='any date 1', autospec=True)
    def test_get_graph_dates(self, mock1, mock2):
        date_manager = GraphDateManager()

        self.assertEqual(
            'any date 1',
            date_manager.get_graph_dates(True, None, None, None)
        )

        self.assertEqual(
            'any date 2',
            date_manager.get_graph_dates(None, 1, None, None)
        )

        now = timezone.now()
        later = now + timezone.timedelta(hours=10)

        self.assertEqual(
            [now, later],
            date_manager.get_graph_dates(None, None, now, later)
        )

    '''
    GraphUtils Tests
    '''
    def test_get_graph_measurement_string(self):
        self.assertEqual(
            'Tensão (V)',
            GraphUtils.get_graph_measurement_string('voltage'))

        self.assertEqual(
            'Corrente (A)',
            GraphUtils.get_graph_measurement_string('current'))

        self.assertEqual(
            'Potência Ativa (kW)',
            GraphUtils.get_graph_measurement_string('active_power'))

        self.assertEqual(
            'Potência Reativa (kVAr)',
            GraphUtils.get_graph_measurement_string('reactive_power'))

        self.assertEqual(
            'Potência Aparente (kVA)',
            GraphUtils.get_graph_measurement_string('apparent_power'))

        self.assertEqual(
            '',
            GraphUtils.get_graph_measurement_string(''))

    def test_get_three_phase_args(self):
        self.assertEqual(
            ["voltage_a", "voltage_b", "voltage_c"],
            GraphUtils.get_three_phase_args('voltage')
        )

        self.assertEqual(
            ["current_a", "current_b", "current_c"],
            GraphUtils.get_three_phase_args('current')
        )

        self.assertEqual(
            ["active_power_a", "active_power_b", "active_power_c"],
            GraphUtils.get_three_phase_args('active_power')
        )

        self.assertEqual(
            ["reactive_power_a", "reactive_power_b", "reactive_power_c"],
            GraphUtils.get_three_phase_args('reactive_power')
        )

        self.assertEqual(
            ["apparent_power_a", "apparent_power_b", "apparent_power_c"],
            GraphUtils.get_three_phase_args('apparent_power')
        )

        self.assertEqual(
            [],
            GraphUtils.get_three_phase_args('')
        )

    '''
    GraphDataManager Tests
    '''
    @mock.patch.object(
        pandas, 'date_range', autospec=True,
        return_value=['x_data'])
    @mock.patch.object(
        GraphUtils, 'get_three_phase_args',
        return_value=['arg 1', 'arg 2'])
    @mock.patch.object(
        GraphDataManager, 'filter_y_data', autospec=True)
    def test_get_graph_data(
        self, mock_filter_y_data, mock_three_phase, mock_date_range
    ):
        data_manager = GraphDataManager()
        initial_date = timezone.now()

        self.create_energy_measurements(
            2, initial_date, minutes=1
        )

        # Testing 1 minute based graph
        mock_filter_y_data.return_value = [['y_data_1'], ['square_indexes_1']]
        self.assertEqual(
            [['x_data'], ['y_data_1'], ['square_indexes_1']],
            data_manager.get_graph_data(
                self.transductor,
                'voltage',
                initial_date,
                initial_date + timezone.timedelta(hours=2)
            )
        )

        # Testing 5 minutes based graph
        mock_filter_y_data.return_value = [['y_data_2'], ['square_indexes_2']]
        self.assertEqual(
            [['x_data'], ['y_data_2'], ['square_indexes_2']],
            data_manager.get_graph_data(
                self.transductor,
                'voltage',
                initial_date,
                initial_date + timezone.timedelta(hours=12)
            )
        )

        # Testing 10 minutes based graph
        mock_filter_y_data.return_value = [['y_data_3'], ['square_indexes_3']]
        self.assertEqual(
            [['x_data'], ['y_data_3'], ['square_indexes_3']],
            data_manager.get_graph_data(
                self.transductor,
                'voltage',
                initial_date,
                initial_date + timezone.timedelta(days=1)
            )
        )

        # Testing 1 hour based graph
        mock_filter_y_data.return_value = [['y_data_4'], ['square_indexes_4']]
        self.assertEqual(
            [['x_data'], ['y_data_4'], ['square_indexes_4']],
            data_manager.get_graph_data(
                self.transductor,
                'voltage',
                initial_date,
                initial_date + timezone.timedelta(days=2)
            )
        )

    def test_filter_y_data_without_range_date(self):
        initial_date = timezone.datetime(year=2017, month=1, day=1)
        final_date = initial_date + timezone.timedelta(minutes=4)

        self.create_energy_measurements(
            2, initial_date, minutes=2
        )

        qs = self.transductor.energymeasurements_set.all()
        args = ["voltage_a", "voltage_b", "voltage_c"]
        kwargs = {'minutes': 1}

        data_manager = GraphDataManager()

        y_data, dict_square = data_manager.filter_y_data(
            False, initial_date, final_date, qs, *args, **kwargs
        )

        self.assertEqual(
            {1: 1, 3: 2},
            dict_square
        )

        ya, yb, yc = numpy.array(y_data).T

        ya_test = numpy.array([122.875, 123.375, 123.875, 123.375, 123.375])
        yb_test = numpy.array([122.784, 123.284, 123.784, 123.284, 123.284])
        yc_test = numpy.array([121.611, 122.111, 122.611, 122.111, 122.111])

        self.assertTrue(
            numpy.array_equal(ya, ya_test)
        )

        self.assertTrue(
            numpy.array_equal(yb, yb_test)
        )

        self.assertTrue(
            numpy.array_equal(yc, yc_test)
        )

    def test_filter_y_data_with_range_date(self):
        initial_date = timezone.datetime(year=2017, month=1, day=1)
        final_date = initial_date + timezone.timedelta(minutes=5)

        self.create_energy_measurements(
            6, initial_date, minutes=1
        )

        qs = self.transductor.energymeasurements_set.all()

        args = ["voltage_a", "voltage_b", "voltage_c"]
        kwargs = {'minutes': 3}

        data_manager = GraphDataManager()

        y_data, dict_square = data_manager.filter_y_data(
            True, initial_date, final_date, qs, *args, **kwargs
        )

        self.assertEqual(
            {},
            dict_square
        )

        ya, yb, yc = numpy.array(y_data).T

        ya_test = numpy.array([124.375, 126.875])
        yb_test = numpy.array([124.284, 126.784])
        yc_test = numpy.array([123.111, 125.611])

        self.assertTrue(
            numpy.array_equal(ya, ya_test)
        )

        self.assertTrue(
            numpy.array_equal(yb, yb_test)
        )

        self.assertTrue(
            numpy.array_equal(yc, yc_test)
        )

    def test_get_graph_data_raise_exception(self):
        data_manager = GraphDataManager()
        initial_date = timezone.now()
        final_date = initial_date + timezone.timedelta(days=1)

        self.create_energy_measurements(
            1, initial_date, minutes=1
        )

        with self.assertRaises(NoMeasurementsFoundException):
            data_manager.get_graph_data(
                self.transductor, 'voltage', initial_date, final_date
            )

    '''
    GraphPlotManager Tests
    '''
    @mock.patch.object(
        GraphDataManager, 'get_graph_data',
        side_effect=NoMeasurementsFoundException('No measurements avaiable'),
        autospec=True)
    def test_create_line_graph_raise_exception(self, mock_get_graph_data):
        plot_manager = GraphPlotManager()

        initial_date = timezone.now()
        final_date = initial_date + timezone.timedelta(days=1)

        with self.assertRaises(NoMeasurementsFoundException):
            plot_manager.create_line_graph(
                self.transductor, 'voltage', initial_date, final_date
            )

    @mock.patch.object(
        GraphPlotManager, 'draw_squares_covering_missing_data',
        return_value=None, autospec=True)
    @mock.patch.object(
        GraphPlotManager, 'annotate_max_min_points',
        return_value=None, autospec=True)
    @mock.patch.object(
        GraphPlotManager, 'set_subplot_legend',
        return_value=None, autospec=True)
    @mock.patch.object(
        GraphPlotManager, 'set_subplot_basic_information',
        return_value=None, autospec=True)
    @mock.patch.object(
        GraphDataManager, 'get_graph_data', autospec=True)
    def test_create_line_graph(
        self, mock_get_graph_data, mock_set_subplot_information,
        mock_set_subplot_legend, mock_annotate, mock_squares
    ):
        x_data = pandas.date_range(
            timezone.now(), periods=2, freq='T'
        )

        mock_get_graph_data.return_value = [
            x_data,
            numpy.array([numpy.array([1, 2, 3]), numpy.array([4, 5, 6])]),
            {}
        ]

        plot_manager = GraphPlotManager()
        json_str = plot_manager.create_line_graph('test', 'test', None, None)

        self.assertIn(
            '1.0, 2.0, 3.0',
            json_str
        )

        self.assertIn(
            '4.0, 5.0, 6.0',
            json_str
        )

    @mock.patch.object(
        GraphUtils, 'get_graph_measurement_string',
        return_value='mocked_string')
    def test_set_subplot_basic_information(self, mock_get_string):
        plot_manager = GraphPlotManager()

        fig = Figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)

        plot_manager.set_subplot_basic_information(ax, 'test')

        self.assertEqual(
            _('Monitoramento de mocked_string'),
            ax.get_title()
        )

        self.assertEqual(
            _('Data'),
            ax.get_xlabel()
        )

        self.assertEqual(
            _('mocked_string'),
            ax.get_ylabel()
        )

    def test_set_subplot_legend(self):
        plot_manager = GraphPlotManager()

        fig = Figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)

        labels = ['A']

        plot_1 = ax.plot([1, 2], [1, 2], '-', color='blue', label=labels[0])

        plot_manager.set_subplot_legend(ax)

        self.assertEqual(
            (plot_1, labels),
            ax.get_legend_handles_labels()
        )

    @mock.patch.object(
        Figure, 'add_subplot', autospec=True)
    def test_annotate_max_min_points(self, mock_add_subplot):
        plot_manager = GraphPlotManager()

        fig = Figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)

        text_mock = mock_add_subplot.return_value \
                                    .text = mock.MagicMock()

        scatter_mock = mock_add_subplot.return_value \
                                       .scatter = mock.MagicMock()

        initial_date = timezone.now()

        x_data = pandas.date_range(
            initial_date, periods=6, freq='1min'
        )

        ya = [1, 2]
        yb = [3, 4]
        yc = [5, 6]

        plot_manager.annotate_max_min_points(
            ax, x_data, ya, yb, yc
        )

        total_calls = 6

        self.assertEqual(text_mock.call_count, total_calls)
        self.assertEqual(scatter_mock.call_count, total_calls)

    @mock.patch.object(
        Figure, 'add_subplot', autospec=True)
    def test_draw_squares(self, mock_add_subplot):
        plot_manager = GraphPlotManager()

        fig = Figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)

        patch_mock = mock_add_subplot.return_value \
                                     .add_patch = mock.MagicMock()

        dict_square_indexes = {1: 1, 2: 1}

        x_data = pandas.date_range(
            timezone.now(), periods=6, freq='1min'
        )

        ya = [1, 2]
        yb = [3, 4]
        yc = [5, 6]

        plot_manager.draw_squares_covering_missing_data(
            ax, dict_square_indexes, x_data, ya, yb, yc
        )

        total_calls = 2

        self.assertEqual(patch_mock.call_count, total_calls)

    @mock.patch.object(
        Figure, 'add_subplot', autospec=True)
    def test_draw_squares_without_dict_square_indexes(self, mock_add_subplot):
        plot_manager = GraphPlotManager()

        fig = Figure(figsize=(10, 5))
        ax = fig.add_subplot(1, 1, 1)

        patch_mock = mock_add_subplot.return_value \
                                     .add_patch = mock.MagicMock()

        dict_square_indexes = {}

        x_data = pandas.date_range(
            timezone.now(), periods=6, freq='1min'
        )

        ya = [1, 2]
        yb = [3, 4]
        yc = [5, 6]

        plot_manager.draw_squares_covering_missing_data(
            ax, dict_square_indexes, x_data, ya, yb, yc
        )

        total_calls = 0

        self.assertEqual(patch_mock.call_count, total_calls)

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building
