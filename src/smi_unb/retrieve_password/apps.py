from django.apps import AppConfig


class RetrievePasswordConfig(AppConfig):
    name = 'retrieve_password'
